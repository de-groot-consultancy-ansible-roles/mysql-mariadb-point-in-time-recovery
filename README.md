# DEPRECATED

This role is deprecated. Instead, use the mariadb-mysql-asynchronous-replica role to set up a point in time recovery replica. Use the dba-toolkit to convert all tables to blackhole.
